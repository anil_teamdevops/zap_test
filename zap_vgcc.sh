#!/bin/sh
set -x

rf -rf scan-report/* > /dev/null
mkdir scan-report > /dev/null

#docker run -v test:/zap/wrk/ -i owasp/zap2docker-stable zap-baseline.py -t "https://flooff.me/" -r baseline-report.html 
#docker run -i owasp/zap2docker-stable zap-full-scan.py -t "https://flooff.me/"

docker rm -f $(docker ps -aq -f name=zap_scanner) > /dev/null
docker run -dit --name zap_scanner -i owasp/zap2docker-stable
docker exec zap_scanner mkdir wrk

#docker exec zap_scanner zap-baseline.py -t "https://dev.vgcc.io/" -r baseline-report.html 
docker exec zap_scanner zap-full-scan.py -t "https://dev.vgcc.io/" -r fullscan-report.html

#docker cp zap_scanner:/zap/wrk/baseline-report.html ./scan-report/
docker cp zap_scanner:/zap/wrk/fullscan-report.html ./scan-report/

docker rm -f zap_scanner
echo $? > /dev/null

